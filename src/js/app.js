import { languageLevel } from "./modules/languageLevel.js";
import { typewriterConfig } from "./modules/typewriterConfig.js";
import { reveal } from "./modules/reveal.js";
import { animatedText } from './modules/animatedText.js';

animatedText();

// Animation menu Hamburger
const hamburgerMenu = document.querySelector(".hamburger-menu");
const navbar = document.querySelector(".navitems-container");
const navbarItems = document.querySelectorAll(".navbar-item");
const logoContainer = document.querySelector(".logo-container");

hamburgerMenu.addEventListener("click", () => {
    hamburgerMenu.classList.toggle("active");
    navbar.classList.toggle("active-navbar");
});

navbarItems.forEach((item) => {
    item.addEventListener("click", () => {
        hamburgerMenu.classList.toggle("active");
        navbar.classList.toggle("active-navbar");
    });
});

logoContainer.addEventListener("click", () => {
    hamburgerMenu.classList.toggle("active");
    navbar.classList.toggle("active-navbar");
});


document.querySelector('.main').addEventListener('click', (e) => {
    hamburgerMenu.classList.remove("active");
    navbar.classList.remove("active-navbar");
})
document.querySelector('.header').addEventListener('click', (e) => {
    hamburgerMenu.classList.remove("active");
    navbar.classList.remove("active-navbar");
})


// Icône Go To Top
const goToTopButton = document.querySelector('.go-to-top');

window.addEventListener('scroll', () => {
    if(document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        goToTopButton.style.right = "35px";
    } else {
        goToTopButton.style.right = "-200px";
    }
});

goToTopButton.addEventListener('click', () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    hamburgerMenu.classList.remove("active");
    navbar.classList.remove("active-navbar");
});


AOS.init(
    {
        duration: 1000,
        easing: 'ease-in-out',
        once: true,
    }
);

reveal();
typewriterConfig();
languageLevel();