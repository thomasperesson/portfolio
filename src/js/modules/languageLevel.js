export const languageLevel = () => {
    const languageLevel = {
        htmlCss: "Maîtrise",
        javaScript: "Autonome",
        php: "Autonome",
        wordPress: "Autonome",
        react: "Débutant",
        symfony: "Débutant",
    };

    const languageLevelCardTexts = document.querySelectorAll(".language-level-card");

    const htmlCssLevel = languageLevelCardTexts[0];
    const javaScriptLevel = languageLevelCardTexts[1];
    const phpLevel = languageLevelCardTexts[2];
    const wordPressLevel = languageLevelCardTexts[3];
    const reactLevel = languageLevelCardTexts[4];
    const symfonyLevel = languageLevelCardTexts[5];

    htmlCssLevel.innerHTML = languageLevel.htmlCss;
    javaScriptLevel.innerHTML = languageLevel.javaScript;
    phpLevel.innerHTML = languageLevel.php;
    wordPressLevel.innerHTML = languageLevel.wordPress;
    reactLevel.innerHTML = languageLevel.react;
    symfonyLevel.innerHTML = languageLevel.symfony;
};
