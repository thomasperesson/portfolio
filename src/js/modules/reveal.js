export const reveal = () => {
    const ratio = 0.1;
    const options = {
        root: null,
        rootMargin: "0px",
        threshold: ratio,
    };

    const handleIntersect = (entries, observer) => {
        entries.forEach((entry) => {
            if (entry.intersectionRatio > ratio) {
                entry.target.classList.add("reveal-visible");
                observer.unobserve(entry.target);
            }
        });
    };

    const observer = new IntersectionObserver(handleIntersect, options);
    const reveal = document.querySelectorAll(".reveal");
    reveal.forEach((r) => {
        observer.observe(r);
    });

    document.addEventListener("DOMContentLoaded", function () {
        const options = {
            type: "fade",
            perPage: 1,
            autoplay: true,
            lazyload: "nearby",
            rewind: true,
            interval: 4000,
        };
    });
    const handleIntersectX = (entries, observer) => {
        entries.forEach((entry) => {
            if (entry.intersectionRatio > ratio) {
                entry.target.classList.add("revealx-visible");
                observer.unobserve(entry.target);
            }
        });
    };

    const observerx = new IntersectionObserver(handleIntersectX, options);
    const revealx = document.querySelectorAll(".revealx");
    revealx.forEach((r) => {
        observerx.observe(r);
    });
};
