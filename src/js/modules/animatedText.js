export const animatedText = () => {
    const spanContainers = document.querySelectorAll('.span-container');
    spanContainers.forEach(spanContainer => {
        const letters = spanContainer.children[0].textContent.split('');
        spanContainer.innerHTML = "";

        letters.forEach((letter, index) => {
            spanContainer.innerHTML += `<span style="transition-delay: ${0.05 * index}s">${letter}</span>`;
        });
    })
}