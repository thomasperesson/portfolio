export const typewriterConfig = () => {
    // Animation écriture
    const animTxt = document.getElementById("typewriter");
    const twOptions = {
        loop: true,
        deleteSpeed: 30,
    };
    const typewriter = new Typewriter(animTxt, twOptions);
    typewriter
        .pauseFor(1500)
        .changeDelay(50)
        .typeString('Développeur <strong style="padding: 0; font-weight: 500; ">Web</strong> !')
        .pauseFor(1000)
        .deleteChars(5)
        .typeString('<strong style="padding: 0; font-weight: 500; ">Web mobile</strong> !')
        .pauseFor(1000)
        .deleteChars(12)
        .typeString('<span style="color: #95a4f5; padding: 0; font-weight: 500; ">PHP</span> !')
        .pauseFor(1000)
        .deleteChars(5)
        .typeString('<span style="color: #00d8ff; padding: 0; font-weight: 500; ">React</span> !')
        .pauseFor(1000)
        .deleteChars(8)
        .typeString('<span style="color: #d1d1d1; padding: 0; font-weight: 500; ">Symfony</span> !')
        .pauseFor(1000)
        .start();
};
